﻿using Microsoft.AspNetCore.Identity;

namespace SimpleSSO.Authentication
{
    public class AppUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
