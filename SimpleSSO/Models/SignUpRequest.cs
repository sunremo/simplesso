﻿namespace SimpleSSO.Models
{
    public class SignUpRequest
    {
        public string UserName { set; get; }
        public string Password { set; get; }
        public string Email { set; get; }
    }
}