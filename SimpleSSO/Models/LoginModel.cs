﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleSSO.Models
{
    public class LoginModel
    {
        public string UserName { set; get; }
        public string Password { set; get; }
    }
}
