﻿namespace SimpleSSO.Models
{
    public class UserModel
    {
        public string Id { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
    }
}
