import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import Home from './components/Home';
import FetchData from './components/FetchData';
import Counter from './components/Counter';
import Login from './components/Login';
import Register from './components/Register';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/counter' component={ Counter } />
    <Route path='/login' component= {Login} />
    <Route path='/register' component={Register} />
    <Route path='/fetchdata/:startDateIndex?' component={ FetchData } />
</Layout>;
