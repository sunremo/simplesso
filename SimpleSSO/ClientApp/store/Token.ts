import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';


//State
export interface TokenState {
    accessToken: string;
    showTokenRequestFaild?: boolean;
}

//Actions
interface RequestTokenAction {
    type: 'REQUEST_TOKEN';
    userName: string;
    password: string;
}

interface RecieveTokenSuccessAction {
    type: 'RECIEVE_TOKEN_SUCCESS',
    token: string
}

interface RecieveTokenFailAction {
    type: 'RECIEVE_TOKEN_FAIL'
}

type KnownAction = RequestTokenAction | RecieveTokenSuccessAction | RecieveTokenFailAction;

export const actionCreators = {
    requestToken: (userName: string, password: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const payload = {
            userName: userName,
            password: password,
        }

        let fetchTask = fetch(`api/token`, {
            method: 'POST',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
        })
            .then(response =>{
                if(response.ok){
                    return response.text() as string;
                }
                else{
                    throw new Error('Something went wrong ...');
                }
            })
            .then(data => {
                localStorage.setItem('accessToken', data);
            }).catch(function() {
                console.log("error");
            });

        addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
        dispatch({ type: 'REQUEST_TOKEN', userName: userName, password: password });
    }
};

const unloadedState: TokenState = { accessToken: '', showTokenRequestFaild: false };

export const reducer: Reducer<TokenState> = (state: TokenState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    
    switch (action.type) {
        case 'REQUEST_TOKEN':
            return {
                showTokenRequestFaild: false,
                accessToken: ''
            };
        case 'RECIEVE_TOKEN_SUCCESS':
            return {
                accessToken: action.token,
                showTokenRequestFaild: false
            };
        case 'RECIEVE_TOKEN_FAIL':
            return {
                showTokenRequestFaild: true,
                accessToken:''
            };
        default:
            // The following line guarantees that every action in the KnownAction union has been covered by a case above
            return unloadedState;
    }
};