import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import * as Token from '../store/Token';
import { ApplicationState } from '../store';
import { addTask } from 'domain-task';


type RegisterProps = RouteComponentProps<{}>

type RegisterState = {
    userName: string;
    password: string;
    email: string;
    showError: boolean;
};

class Register extends React.Component<RegisterProps, RegisterState> {
    constructor(props: RegisterProps) {
        super(props);

        this.state = { password: '', userName: '', email: '', showError: false };
        this.register = this.register.bind(this);
    }
    register() {
        const payload = {
            userName: this.state.userName,
            password: this.state.password,
            email: this.state.email
        }
        let fetchTask = fetch(`api/signup`, {
            method: 'POST',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
        })
            .then(response => {
                if (response.ok) {
                    return response.text();
                }
                else {
                    throw new Error('Something went wrong ...');
                }
            })
            .then(data => {
                localStorage.setItem('accessToken', data);
                this.props.history.push('/login');
            }).catch(() => {
                console.log("error");
                this.setState({ showError: true });
            });

        addTask(fetchTask);
    }
    handleChange(event: any) {
        this.setState({ [event.target.name]: event.target.value });
    }
    renderError() {
        if(this.state.showError){
            return (<div className="alert alert-danger" role="alert">
                Registration failed!
        </div>);
        }
    }

    public render() {
        return (<div>
            <h1>Registration form</h1>
            {this.renderError()}
            <div className="form-group">
                <input type="text" className="form-control"
                    id="email"
                    name="email"
                    placeholder="Enter email"
                    onChange={e => this.handleChange(e)}
                />
            </div>
            <div className="form-group">
                <input type="text" className="form-control"
                    id="userName"
                    name="userName"
                    placeholder="Enter userName"
                    onChange={e => this.handleChange(e)}
                />
            </div>
            <div className="form-group">
                <input type="password" className="form-control"
                    id="password"
                    name="password"
                    placeholder="Enter password"
                    onChange={e => this.handleChange(e)}
                    ref={this.state.password} />
            </div>
            <button onClick={this.register} className="btn btn-primary">Register</button>

        </div >

        );
    }
}

export default Register;
