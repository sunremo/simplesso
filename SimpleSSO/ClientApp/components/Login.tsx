import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import * as Token from '../store/Token';
import { ApplicationState } from '../store';
import { addTask } from 'domain-task';


type LoginProps = RouteComponentProps<{}>

type LoginState = {
    userName: string;
    password: string;
    showTokenRequestFaild: boolean;
};

class Login extends React.Component<LoginProps, LoginState> {
    constructor(props: LoginProps) {
        super(props);
        this.state = { password: '', userName: '', showTokenRequestFaild: false };

        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
    }
    login() {
        const payload = {
            userName: this.state.userName,
            password: this.state.password,
        }

        let fetchTask = fetch(`api/signin`, {
            method: 'POST',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
        })
            .then(response => {
                if (response.ok) {
                    return response.text();
                }
                else {
                    throw new Error('Something went wrong ...');
                }
            })
            .then(data => {
                localStorage.setItem('accessToken', data);
                this.props.history.push('/');
            }).catch(() => {
                console.log("error");
                this.setState({ showTokenRequestFaild: true });
            });

        addTask(fetchTask);
    }
    handleChange(event: any) {
        this.setState({ [event.target.name]: event.target.value });
    }
    renderError() {
        if (this.state.showTokenRequestFaild) {
            return (<div className="alert alert-danger" role="alert">
                Username or password is not valid!
        </div>);
        }
    }

    public render() {
        return (
            <div>
                <h1>Login form</h1>
                {this.renderError()}
                <div className="form-group">
                    <input type="text" className="form-control"
                        id="userName"
                        name="userName"
                        placeholder="Enter userName"
                        onChange={e => this.handleChange(e)}
                    />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control"
                        id="password"
                        name="password"
                        placeholder="Enter password"
                        onChange={e => this.handleChange(e)}
                        ref={this.state.password} />
                </div>
                <button onClick={this.login} className="btn btn-primary">Login</button>

            </div>

        );
    }
}

export default Login;
