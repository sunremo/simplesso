﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SimpleSSO.Authentication;
using SimpleSSO.Models;

namespace SimpleSSO.AutoMapper
{
    public class DataLayerProfile : Profile
    {
        public DataLayerProfile()
        {
            CreateMap<AppUser, UserModel>();
        }
    }
}
