﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleSSO.Models;

namespace SimpleSSO.Controllers
{
    public class UserController: Controller
    {
        //TODO: move it to the data layer
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;

        public UserController(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet("api/user")]
        public async Task<IEnumerable<UserModel>> GetUsers()
        {
            var dbUsers = await _dbContext.Users.ToListAsync();
            var result = dbUsers.Select(u => _mapper.Map<UserModel>(u));
            return result;
        }
    }
}
