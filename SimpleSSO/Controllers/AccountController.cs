﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SimpleSSO.Authentication;
using SimpleSSO.Models;

namespace SimpleSSO.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly UserManager<AppUser> _userManager;

        public AccountController(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("api/SignIn")]
        public async Task<IActionResult> SingIn([FromBody]LoginModel login)
        {
            var user = await _userManager.FindByNameAsync(login.UserName);

            if (user == null || !await _userManager.CheckPasswordAsync(user, login.Password))
                return BadRequest();

            // authentication successful so generate jwt token
            var claims = new List<Claim>()
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(issuer:AuthOptions.ISSUER,
                                            audience:AuthOptions.AUDIENCE,
                                            notBefore:DateTime.UtcNow,
                                            claims: claims,
                                            expires:now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                                            signingCredentials:new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),SecurityAlgorithms.HmacSha256));

            return Ok(tokenHandler.WriteToken(jwt));
        }


        [HttpPost("api/SignUp")]
        public async Task<IActionResult> Post([FromBody]SignUpRequest model)
        {
            //TODO: add model and data validation
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AppUser appUser = new AppUser()
            {
                UserName = model.UserName,
                Email = model.Email
            };
            
            var result = await _userManager.CreateAsync(appUser, model.Password);
            if (result.Succeeded)
            {
                return Ok();
            }

            return BadRequest("Password is not strong.");
        }
    }
}
