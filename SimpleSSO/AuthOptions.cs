﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace SimpleSSO
{
    public class AuthOptions
    {
        public const string ISSUER = "SimpleSSO";
        public const string AUDIENCE = "http://localhost:23015/";
        private const string KEY = "super_secure_secret_key";
        public const int LIFETIME = 10;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
        }
    }
}
